FROM ubuntu:18.04

RUN apt-get update
RUN apt-get -y install curl gnupg
RUN curl -sL https://deb.nodesource.com/setup_15.x  | bash -
RUN apt-get -y install nodejs
# RUN npm install
RUN apt install chromium-browser -y
RUN apt-get --fix-broken install

WORKDIR /app

COPY . .

RUN npm i

ENV PORT=8080

EXPOSE 8080

RUN groupadd -r pptruser && useradd -r -g pptruser -G audio,video pptruser \
    && mkdir -p /home/pptruser/Downloads \
    && chown -R pptruser:pptruser /home/pptruser

USER pptruser

CMD ["node", "server"]
