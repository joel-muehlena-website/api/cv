const express = require("express");
const router = express.Router();
const path = require("path");
const puppeteer = require("puppeteer");
const hbs = require("handlebars");
const fs = require("fs-extra");
const axios = require("axios");

const compileHbsToHtml = async (templateName, data) => {
  const filePath = path.join(process.cwd(), "templates", `${templateName}.hbs`);
  const html = await fs.readFile(filePath, "utf-8");
  return hbs.compile(html)(data);
};

hbs.registerHelper("experienceType", (number) => {
  state = "";
  switch (number) {
    case 0:
      state = "Internship";
      break;
    case 1:
      state = "Job";
      break;
  }
  return state;
});

hbs.registerHelper("graduateState", (number) => {
  state = "";
  switch (number) {
    case 0:
      state = "not graduated";
      break;
    case 1:
      state = "not yet graduated (still on it)";
      break;
    case 2:
      state = "graduated";
      break;
  }
  return state;
});

const getMonthNameById = (id) => {
  const getMonthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  if (id > getMonthNames.length || id < 0) {
    throw Error("No valid month provided");
  }

  return getMonthNames[id];
};

hbs.registerHelper("displayDateFromUTC", (date) => {
  const newDate = new Date(date);
  const year = newDate.getFullYear();
  const month = newDate.getMonth();
  let monthName = getMonthNameById(month);

  return monthName + " " + year;
});

router.get("/", async (_, res) => {
  try {
    const browser = await puppeteer.launch({
      args: ["--no-sandbox"],
    });
    const page = await browser.newPage();
    const cssStyle = path.join(process.cwd(), "templates", "cv-eng-pdf.css");

    const host = "http://api-gateway.jm-api.svc.cluster.local:80";

    const edu = await axios.get(`${host}/v1/education`);

    const skills = await axios.get(`${host}/v2/skill`);

    const xp = await axios.get(`${host}/v1/experience`);

    const data = {
      name: "Joel Mühlena",
      address: ["Weserstr. 5", "63225 Langen"],
      phone: "+49 152 / 21780140",
      email: "joel.rene (at) muehlena.de",
      homepage: "joel.muehlena.de",
      imageLink: "https://i.ibb.co/xJSWJJj/joel-2018-bewerbung-1mb-comp.jpg",
      skill: skills.data.data,
      education: edu.data.data,
      experience: xp.data.data,
      other: [
        "Teamer at the AWO and the deanery",
        "Sound and PA for school events",
      ],
      hobby: ["Programming", "Videos", "Computer", " Judo (4. Kyu)"],
    };

    const content = await compileHbsToHtml("cv-eng-pdf", data);

    await page.setContent(content);
    await page.emulateMediaType("print");
    await page.addStyleTag({ path: cssStyle });
    let buffer = await page.pdf({
      format: "A4",
      displayHeaderFooter: false,
      printBackground: true,
      margin: {
        top: 30,
        bottom: 10,
      },
    });

    await browser.close();

    res.type("application/pdf");
    res.send(buffer);
  } catch (err) {
    res.status(500).json({
      code: 500,
      msg: "There was an error generating the pdf",
      error: err,
    });
  }
});

module.exports = router;
