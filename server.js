const express = require("express");
const app = express();
const cors = require("cors");

const PORT = process.env.PORT || 3003;

app.use(cors());

const cvRoute = require("./routes/cv");
app.use("/v1/cv", cvRoute);

app.get("/healthz", (_, res) => {
  res.sendStatus(200);
});

app.listen(PORT, () => {
  console.log(`Server läuft auf port: ${PORT}`);
});
